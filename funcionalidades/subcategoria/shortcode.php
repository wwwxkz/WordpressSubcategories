<?php

function sna_subcategoria_shortcode() 
{
    $retorno = '';

    $id = 9;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }
    $args = array('parent' => $id);
    $categories = get_categories($args);
    $retorno .= '<h2 class="categorias-header titulo">Parcerias e convênios</h2>';
    $retorno .= '<h4 class="categorias-header">Selecione uma categoria para quais empresas ofereçam descontos aos associados</h4>';
    $retorno .= '<div class="categorias">';
    foreach($categories as $category) {
        $retorno .= '<div class="categoria principal">';
        $retorno .= '<div>';
        $retorno .= '<img src="..\wp-content\plugins\sna_subcategorias\funcionalidades\subcategoria\assets\imgs' . strip_tags(category_description( $category->term_id )) . '"height="30px" style="margin:15px;"/>';
        $retorno .= '</div>';
        $retorno .= '<div>';
        $retorno .= '<a href="' . get_category_link( $category->term_id ) . '">' . $category->name.'</a>';
        $retorno .= '</div>';
        $retorno .= '</div>';
    }    
    $retorno .= '</div>';

    return $retorno;
}

?>
