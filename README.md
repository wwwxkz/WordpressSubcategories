# WordpressSubcategories

* This plugin depends on https://github.com/wwwxkz/WordpressThemeSNA to properly work, you can use it without it, but will have to make some adjustments.

![](https://github.com/wwwxkz/WordpressSubcategories/blob/main/README/4.png)
![](https://github.com/wwwxkz/WordpressSubcategories/blob/main/README/2.png)
![](https://github.com/wwwxkz/WordpressSubcategories/blob/main/README/3.png)
![](https://github.com/wwwxkz/WordpressSubcategories/blob/main/README/1.png)

